/*
 * types.h
 *
 *  Created on: 21.02.2013
 *      Author: Sky-WaLkeR
 */

#ifndef TYPES_H_
#define TYPES_H_

#ifndef uchar
#define uchar unsigned char
#endif
#ifndef uint
#define uint unsigned int
#endif
#ifndef ulong
#define ulong unsigned long
#endif
#ifndef schar
#define schar signed char
#endif

#endif /* TYPES_H_ */
