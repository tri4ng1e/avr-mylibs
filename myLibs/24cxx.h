#ifndef _LIB_24CXX_H_
#define _LIB_24CXX_H_

/* Description

     Author: Sky-WaLkeR
       Date: 30.04.13 23:43 ( UTC+4 )
    Version: 1.14
     Tested: ATmega8 @ 8MHz

	Description: 
   This library allows you to work with External EEPROM module 24C16..64

	Changelog:
   1.14
     [R]emoved dependency from types.h
   1.11
     [A]dded check for reinit and force reinit
   1.10
     [R]emoved dependency from util/delay.h library
     [S]olved some little bugs

*/

#ifndef _LIB_TWI_H_
#warning "Library \"24Cxx\" has dependency from libTWI.h. This lib auto-included."
#include <myLibs/libTWI.h>
#endif

#ifndef _24CXX_ADDR
#define _24CXX_ADDR 0xA0
#endif

// for internal use only
void __eep_delay( uint32_t t) {
	while (t!=0){ asm("nop"); t--; }
}

/* init work with EEPROM
 Such construction disallows reinit -> no time waste
 If you need to force reinit bus - give argument "1"
*/
void eep_init( uint8_t force=0 ){
	if (i2c_inited|force==0) i2c_init();
}

// write byte to given address
void eep_write_byte( uint16_t addr, uint8_t data ){
	i2c_start_wait(_24CXX_ADDR|I2C_WRITE);
	i2c_write(addr>>8);
	i2c_write(addr&(0xff));
	i2c_write(data);
	i2c_stop();
}

// read byte from address
uint8_t eep_read_byte( uint16_t addr ){
	uint8_t data;
	i2c_start_wait(_24CXX_ADDR|I2C_WRITE);
	i2c_write(addr>>8);
	i2c_write((char)addr);
	i2c_start_wait(_24CXX_ADDR|I2C_READ);
	data=i2c_readNak();
	i2c_stop();
	return data;
}

// EEPROM is much slower than MCU, then we must give him some time to operate
// blocking function
void eep_wait(){
	while (i2c_start(_24CXX_ADDR)==1) __eep_delay(1000/(1000000UL/F_CPU));
}

// write byte and wait
void eep_write_byte_wait( uint16_t addr, uint8_t data ){
	eep_write_byte(addr, data);
	eep_wait();
}


#endif /* 24C32_H_ */
