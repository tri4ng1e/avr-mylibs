#ifndef _LIB_nRF24_H_
#define _LIB_nRF24_H_

#include <myLibs/types.h>
#include <myLibs/nrf24_defs.h>

#include <myLibs/libSPI.h>
#include <avr/interrupt.h>

/* ATTENTION!
    If you want to decrease size of HEX, add these parameters in Toolchain's settings:
     Compiler: "-ffunction-sections -fdata-sections"  (without quotes)
     Linker: "-Wl,--gc-sections"  (without quotes)
    This will delete all unused functions from binary file.
 */

/* !WARNING!
	I don't take any responsibility for this library
*/

/* Description

     Author: Sky-WaLkeR, based on Tinkerer's lib
       Date: 09.03.13 21:38
    Version: 1.31
     Tested: ATmega8 @ 8MHz

    Description: This library provides user-friendly work with nRF24L01+ transceiver. Code based on Tinkerer's Lib and
     uses it's "spi.h" library. Code is highly rewritten (but interrupt handler not - in future): added MultiCeiver
     functionality (work with all 6 pipes), user-friendly setup funcs and many other.

     Now INT0 (external interrupt) is required for normal work, but funcs without this dependency
     will be written soon (one of them - mirf_send_wait)

    Funcs: ( * - new func, # - not tested )
     HIGH-level:
      mirf_init() - sets MiRF to default vals, turns on sei and other init fncs
      
      mirf_set_TADDR( addr ) - sets transmit (target) address
      mirf_set_RADDR( addr ) - sets receive address on pipe0
    * mirf_set_RADDR_p1( addr ) - sets address on pipe1
    * mirf_set_pipe_addr( p_num, addr ) - sets LSbyte of address for pipe2-5

    * mirf_set_speed( speed ) - sets speed. 250Kbit, 1Mbit, 2Mbit
    * mirf_set_ch( channel ) - sets channel. 1-125
    * mirf_set_payload( p_num, p_size ) - sets size of payload on selected pipe
    * mirf_set_retr( count, delay ) - sets retranslation parameters
    * mirf_enable_pipes( uchar flag ) - enables or disables rx on pipe0-5 ()

      mirf_send( data, length) - sends data (length==payload)
    * mirf_send_wait( data, length) - send data and waits (blocking func)
      uchar mirf_get_data( buffer ) - recv data from rx_fifo to buffer (return value=pipe number)
      uchar mirf_data_ready() - 1 if rx_buffer not empty

    * mirf_powerdown() - mirf goes to sleep (power-down mode)
    * mirf_powerup() - mirf wakes up (5ms, standby-1)

     LOW-level:
      mirf_config_register( reg, data) - writes data to reg
      mirf_read_register( reg, buf, lenth) - reads reg in buf
      mirf_write_register( reg, data, lenth ) - writes data to reg
    * uchar mirf_read_status() - returns STATUS
    * uchar mirf_read_config() - returns CONFIG

      ISR(INT0_vect) - interrupt handler


    Connections: ( in [brackets] - pins for ATmega8 )
      nRF |  AVR
    -------------
      CE  | define below
     CSN  | define below
     SCK  | SPI  SCK [PB5]
     MOSI | SPI MOSI [PB3]
     MISO | SPI MISO [PB4]
     IRQ  | INT0     [PD2]
     3.3V and GND - to power supply (ONLY 3.3) and ground
      WARNING: logic pins are 5V tolerant, but not power supply!

*/

// Mirf default values
#define mirf_def_CH         2
#define mirf_def_PAYLOAD    16
uchar   mirf_def_CONFIG =   ( (1<<MASK_RX_DR) | (1<<EN_CRC) | (0<<CRCO) );

// Pin definitions for chip select and chip enabled of the MiRF module
// PORTB  (in future - may choose)
#define CE  1
#define CSN 2

// prototypes of funcs needed for defines but written after them - for cvAVR compilator
uchar mirf_read_config();

#define mirf_CSN_hi     PORTB |=  (1<<CSN);
#define mirf_CSN_lo     PORTB &= ~(1<<CSN);
#define mirf_CE_hi      PORTB |=  (1<<CE);
#define mirf_CE_lo      PORTB &= ~(1<<CE);
#define TX_POWERUP		mirf_config_register(CONFIG, mirf_def_CONFIG | ( (1<<PWR_UP) | (0<<PRIM_RX) ) )
#define RX_POWERUP		mirf_config_register(CONFIG, mirf_def_CONFIG | ( (1<<PWR_UP) | (1<<PRIM_RX) ) )
#define POWERDOWN		mirf_config_register(CONFIG, mirf_read_config() & ~(1<<PWR_UP) )
#define POWERUP			mirf_config_register(CONFIG, mirf_read_config() | (1<<PWR_UP) )
uchar mode; // 1 - tx, 0 - rx
uchar payload_size[6]; // for all pipes
uchar data_sent;

// Reads an array of bytes from the given start position in the MiRF registers.
void mirf_read_register(uchar reg, uchar * value, uchar len){
    mirf_CSN_lo;
    spi_fast_shift(R_REGISTER | (REGISTER_MASK & reg));
    spi_transfer_sync(value,value,len);
    mirf_CSN_hi;
}
// Writes an array of bytes into inte the MiRF registers.
void mirf_write_register(uchar reg, uchar * value, uchar len) {
    mirf_CSN_lo;
    spi_fast_shift(W_REGISTER | (REGISTER_MASK & reg));
    spi_transmit_sync(value,len);
    mirf_CSN_hi;
}
// Clocks only one byte into the given MiRF register
void mirf_config_register(uchar reg, uchar value){
    mirf_CSN_lo;
    spi_fast_shift(W_REGISTER | (REGISTER_MASK & reg));
    spi_fast_shift(value);
    mirf_CSN_hi;
}
uchar mirf_read_status(){
    static uchar status;
    mirf_CSN_lo; status = spi_fast_shift(NOP); mirf_CSN_hi; 
    return status;
}
uchar mirf_read_config(){
    static uchar config;
    mirf_read_register(CONFIG, &config, 1);
    return config;
}

// Sets the receiving address
void mirf_set_RADDR(uchar * adr) {
    mirf_CE_lo;
    mirf_write_register(RX_ADDR_P0,adr,5);
    mirf_CE_hi;
}
void mirf_set_RADDR_p1(uchar * adr) {
    mirf_CE_lo;
    mirf_write_register(RX_ADDR_P1,adr,5);
    mirf_CE_hi;
}
void mirf_set_pipe_addr(uchar p_num, uchar addr){
    mirf_CE_lo;
    mirf_config_register(RX_ADDR_P0+p_num, addr);
    mirf_CE_hi;
}
 
// Sets the transmitting address
void mirf_set_TADDR(uchar * adr) {
    mirf_write_register(TX_ADDR, adr,5);
}


// Initializes pins ans interrupt to communicate with the MiRF module
// Should be called in the early initializing phase at startup.
void mirf_init() {
    DDRB |= ((1<<CSN)|(1<<CE));
    mirf_CE_lo;
    mirf_CSN_hi;

    // Set RF channel
    mirf_config_register(RF_CH,mirf_def_CH);

    // Set length of incoming payload 
    mirf_config_register(RX_PW_P0, mirf_def_PAYLOAD);
    payload_size[0]=mirf_def_PAYLOAD;

    mode=0;
    RX_POWERUP;

    mirf_CE_hi;

    // interrupt on O=INT0, Falling edge
    GICR|=0x40;
    MCUCR=0x02;
    GIFR=0x40;

    asm("sei");
}

void mirf_send(uchar * value, uchar len){
    while (mode==1){;} // Wait until last paket is send
 
    mirf_CE_lo;
 
    mode = 1;
    TX_POWERUP;
      
    mirf_CSN_lo;
    spi_fast_shift( FLUSH_TX ); // Write cmd to flush tx fifo
    mirf_CSN_hi;
      
    mirf_CSN_lo;
    spi_fast_shift( W_TX_PAYLOAD ); // Write cmd to write payload
    spi_transmit_sync(value,len);   // Write payload
    mirf_CSN_hi;
      
    mirf_CE_hi; // Start transmission
}
void mirf_send_wait(uchar * value, uchar len){
    while (mode==1){;} // Wait until last paket is send
    mirf_CE_lo;

    mode = 1;
    TX_POWERUP;

    mirf_CSN_lo;
    spi_fast_shift( FLUSH_TX ); // Write cmd to flush tx fifo
    mirf_CSN_hi;

    mirf_CSN_lo;
    spi_fast_shift( W_TX_PAYLOAD ); // Write cmd to write payload
    spi_transmit_sync(value,len);   // Write payload
    mirf_CSN_hi;

    mirf_CE_hi; // Start transmission
    while ((mirf_read_status()&(1<<5|1<<2))==0) {;}
    mirf_CE_lo; RX_POWERUP; mirf_CE_hi; mode = 0;
    mirf_config_register(STATUS,(1<<TX_DS)|(1<<MAX_RT));
}

 
// Reads mirf_PAYLOAD bytes into data array
uchar mirf_get_data(uchar * data) {
    static uchar p_num;
    p_num=mirf_read_status();
    p_num=(p_num&(1<<1|1<<2|1<<3))>>1;

    mirf_CSN_lo;                               // Pull down chip select
    spi_fast_shift( R_RX_PAYLOAD );            // Send cmd to read rx payload
    spi_transfer_sync(data,data,16); // Read payload
    mirf_CSN_hi;                               // Pull up chip select

    mirf_CSN_lo;
    mirf_config_register(STATUS,(1<<RX_DR));   // Reset status register
    mirf_CSN_hi;
    return p_num;
}

// Checks if data is available for reading
uchar mirf_data_ready() {
    if (mode==1) return 0;
    static uchar status;
    // Read MiRF status 
    mirf_CSN_lo;                                // Pull down chip select
    status = spi_fast_shift(NOP);               // Read status register
    mirf_CSN_hi;                                // Pull up chip select
    return status & (1<<RX_DR);
}


ISR(INT0_vect){
    // If still in transmitting mode then finish transmission
    if (mode==1) {
    
        // Read MiRF status 
        mirf_CSN_lo;                                // Pull down chip select
        spi_fast_shift(NOP);               // Read status register
        mirf_CSN_hi;                                // Pull up chip select

        mirf_CE_lo;                             // Deactivate transreceiver
        RX_POWERUP;                             // Power up in receiving mode
        mirf_CE_hi;                             // Listening for pakets
        mode = 0;                                // Set to receiving mode

        // Reset status register for further interaction
        mirf_config_register(STATUS,(1<<TX_DS)|(1<<MAX_RT)); // Reset status register
    }
}


void mirf_set_speed(uchar spd){
    mirf_CE_lo;
    static uchar tmp; mirf_read_register(RF_SETUP, &tmp, 1);
    switch (spd){
      case SPD_250K:
        tmp|=1<<5; tmp&=~(1<<3); break;
      case SPD_1M:
        tmp&=~(1<<5|1<<3); break;
      case SPD_2M:
        tmp|=1<<3; tmp&=~(1<<5); break;
    }
    mirf_config_register(RF_SETUP, tmp);
    mirf_CE_hi;
}

void mirf_set_ch(uchar ch){
    mirf_CE_lo;
    mirf_config_register(RF_CH, ch);
    mirf_CE_hi;
}

void mirf_set_payload( uchar p_num, uchar p_size ){
    p_num=RX_PW_P0+p_num; // for example: if p_num=2, addr=0x11+0x02=0x13 
    mirf_config_register(p_num, p_size);
    payload_size[p_num]=p_size;
}

void mirf_set_retr( uchar count, uchar delay){
    if(count>15||delay>15) return;
    mirf_config_register(SETUP_RETR, count|delay<<4);
}

void mirf_enable_pipes( uchar flag ){
    mirf_CE_lo;
    mirf_config_register(EN_RXADDR, flag);
    mirf_CE_hi;
}

void mirf_powerdown(){
	uchar tmp=mirf_def_CONFIG;
	tmp&=~(1<<PWR_UP);
	mirf_config_register(CONFIG, tmp);
}
void mirf_powerup(){
	uchar tmp=mirf_def_CONFIG;
	tmp|=1<<PWR_UP;
	mirf_config_register(CONFIG, tmp);
	_delay_ms(5);
    mirf_CE_lo; mirf_CSN_hi;
}


#endif
