/*
 * 		bit.h
 * 	Author: Sky-WaLkeR
 * 	
 * 	This file contains defines for bit operations 
 * 
 */
#include <myLibs/types.h>

#define bitHi(a,b) (a|=1<<b)
#define bitLo(a,b) (a&=~(1<<b))

#define bitGet(a,b) ((a&(1<<b))>>b)

#define bitIsHi(a,b) (bitGet(a,b))
#define bitIsLo(a,b) (!bitGet(a,b))

// identical macroses but diffent names
#define bitSet(a,b) bitHi(a,b)
#define bitClr(a,b) bitLo(a,b)

#define bitIsSet(a,b) bitIsHi(a,b)
#define bitIsClr(a,b) bitIsLo(a,b)
