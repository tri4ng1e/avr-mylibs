/*

		libTWI

	Author: Sky-WaLkeR
	 Version: 1.3
   	  Date: 06.05.2013 22:15

	  Description:
	This tibrary allows you to use hardware TWI (I2C) protocol.

*/
#ifndef _LIB_TWI_H_
#define _LIB_TWI_H_
#define TWI_MODE_HARDWARE

#include <util/twi.h>
#include <avr/io.h>
#include <myLibs/types.h>

#define SCL_CLOCK  100000L //I2C clock in Hz

#define I2C_WRITE	0
#define I2C_READ	1

#define I2C_ACK   1
#define I2C_NAK  0

void i2c_init(void) {
  TWSR = 0; /* no prescaler */
  TWBR = ((F_CPU/SCL_CLOCK)-16)/2; /* must be > 10 for stable operation */
}

// Sends START cond, address and direction
// return 0 - success, 1 - failed to access device
uchar i2c_start(uchar address, uchar dir=I2C_WRITE){
    uint twst;
	// send START condition
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);

	// wait until transmission completed
	while(!(TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ( (twst != TW_START) && (twst != TW_REP_START)) return 1;

	// send device address
	TWDR = address|dir;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wail until transmission completed and ACK/NACK has been received
	while(!(TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) ) return 1;

	return 0;
}

// Sends START cond and address (with direction)
// If device is busy, use ack polling to wait until device is ready
void i2c_start_wait(uchar address){
    uint twst;
    while ( 1 ){
	    // send START condition
	    TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);

    	// wait until transmission completed
    	while(!(TWCR & (1<<TWINT)));

    	// check value of TWI Status Register. Mask prescaler bits.
    	twst = TW_STATUS & 0xF8;
    	if ( (twst != TW_START) && (twst != TW_REP_START)) continue;

    	// send device address
    	TWDR = address;
    	TWCR = (1<<TWINT) | (1<<TWEN);

    	// wail until transmission completed
    	while(!(TWCR & (1<<TWINT)));

    	// check value of TWI Status Register. Mask prescaler bits.
    	twst = TW_STATUS & 0xF8;
    	if (( twst==TW_MT_SLA_NACK )||( twst ==TW_MR_DATA_NACK )){
    	    /* device busy, send stop condition to terminate write operation */
	        TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);

	        // wait until stop condition is executed and bus released
	        while(TWCR & (1<<TWSTO));

    	    continue;
    	}
    	//if( twst != TW_MT_SLA_ACK) return 1;
    	break;
     }
}/* i2c_start_wait */


/*************************************************************************
 Issues a repeated start condition and sends address and transfer direction

 Input:   address and transfer direction of I2C device

 Return:  0 device accessible
          1 failed to access device
*************************************************************************/
uchar i2c_rep_start(unsigned char address){
    return i2c_start( address );

}/* i2c_rep_start */


/*************************************************************************
 Terminates the data transfer and releases the I2C bus
*************************************************************************/
void i2c_stop(void){
    /* send stop condition */
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);

	// wait until stop condition is executed and bus released
	while(TWCR & (1<<TWSTO));
}/* i2c_stop */


/*************************************************************************
  Send one byte to I2C device

  Input:    byte to be transfered
  Return:   0 write successful
            1 write failed
*************************************************************************/
uchar i2c_write(uchar data)
{
    uint8_t   twst;

	// send data to the previously addressed device
	TWDR = data;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wait until transmission completed
	while(!(TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits
	twst = TW_STATUS & 0xF8;
	if( twst != TW_MT_DATA_ACK) return 1;
	return 0;
}/* i2c_write */

/*************************************************************************
 Read one byte from the I2C device and send Ack or Nak

 Input:   mode (I2C_ACK or I2C_NAK ), default I2C_ACK
 Return:  byte read from I2C device
*************************************************************************/
uchar i2c_read(uchar mode=I2C_ACK){
	TWCR = (1<<TWINT) | (1<<TWEN) | (mode<<TWEA);
	while(!(TWCR & (1<<TWINT)));

    return TWDR;
}/* i2c_read */


#endif
