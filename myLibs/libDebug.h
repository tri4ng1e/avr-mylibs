/* Description

     Author: Sky-WaLkeR
       Date: 06.05.13 22:17
    Version: 1.01
     Tested: ATmega8 @ 8MHz

	Description:
   This library allows you easily put data (numbers, chars, strings) through UART

   Default UART baud rate is 38400, but you can easily change it - in your main file #define DEB_SPEED (with UL postfix)

	Functions:
   debug_init() - you must call in early beginning
   debug( data ) - sends data through UART
	data can be char, char array, int, long
	if something goes bad - put explicit cast, like debug((char)125);   

	Bugs: (P - presents, S - solved, WIP - work in progress)
   Not found

	Changelog:
   1.02
	 [R]emoved non-used includes
	 [R]emoved unsigned char (now using uint8_t)
   1.01
     [R]emoved non-used includes

*/

#ifndef LIB_DEBUG_H_
#define LIB_DEBUG_H_

#include <stdlib.h>

char debugBuf[10];

#ifndef F_CPU
#error Please set F_CPU!
#endif
#ifndef DEB_SPEED
#define DEB_SPEED 38400UL
#endif

void debug_init(){
	int UBRR_VAL = ( F_CPU /( DEB_SPEED * 16 ) ) - 1;
	UBRRH=UBRR_VAL<<8; UBRRL=UBRR_VAL; // speed
	UCSRC=1<<URSEL|1<<UCSZ1|1<<UCSZ0; // 8 bits
	UCSRB=1<<TXEN;
}

void debug_send_char(char chr){
	while( ( UCSRA & ( 1 << UDRE ) ) == 0  );
	UDR = chr;
}
void debug_send_str( const char *str ){
	uint8_t c;
	while( ( c=*str++ )!=0 ){
		debug_send_char(c);
		c=0;
	}
}
void debug_send_num(long a){
	itoa(a, debugBuf, 10);
	debug_send_str(debugBuf);
}

void debug(char chr){ debug_send_char(chr); }
void debug(const char *str){ debug_send_str(str); }
void debug(long num){ debug_send_num(num); }
void debug(int num){ debug_send_num((long)num); }


#endif /* LIBDEBUG_H_ */
