#ifndef SPI_PROGRAM_LITE_H_
#define SPI_PROGRAM_LITE_H_

#include <myLibs/bit.h>

/*
		libSPI_program
	Author: Sky-WaLkeR

	  Description:
	This library provides the program emulation of SPI protocol.
	Library don't finished yet.

*/

// pin settings
#ifndef SPI_PORT
#warning You didn't choose SPI_PORT and/or other defines. Set to default
#define SPI_PORT PORTB
#define SPI_DDR  DDRB
#define SCK  0
#define MISO 1
#define MOSI 2
#endif

#define SCK_hi bitHi(SPI_PORT, SCK)
#define SCK_lo bitLo(SPI_PORT, SCK)
#define SCK_pulse SCK_hi;SCK_lo

#define OUT_hi bitHi(SPI_PORT, MOSI)
#define OUT_lo bitLo(SPI_PORT, MOSI)
#define IN_get bitGet(SPI_PORT, MISO)


void spi_init(){
	SPI_PORT&=~(1<<SCK|1<<MOSI);
	SPI_DDR|=1<<SCK|1<<MOSI; SPI_DDR&=~(1<<MISO);
}

// send 1 byte and receive 1 byte
uint8_t spi_fast_shift(uint8_t send){
	uint8_t recv=0;
	for(signed char i=7; i>=0; i--){
		if (bitGet(send, i)==1) OUT_hi;
		else OUT_lo;
		SCK_hi;
		recv|=1<<IN_get;
		SCK_lo;
	}
	return recv;
}

// send array of bytes
void spi_transmit_sync(uint8_t * dataout, uint8_t len){
	for (uint8_t i = 0; i < len; i++) {
		for(signed char j=7; j>=0; j--){
			if (bitGet(dataout[i], i)==1) OUT_hi;
			else OUT_lo;
			SCK_pulse;
		}
	}
}

#endif /* SPI_PROGRAM_LITE_H_ */
