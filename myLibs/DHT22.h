#ifndef _LIB_DHT22_H_
#define _LIB_DHT22_H_

#include <avr/io.h>
#include <util/delay.h>

#include <myLibs/bit.h>

/* Description

     Author: Sky-WaLkeR
       Date: 06.05.13 21:57
    Version: 1.1
     Tested: ATmega8 @ 8MHz

	Description: 
   This library allow you to work with temperature and humidity sensor - DHT22 (AM2302, RHT03)

	Changelog:
   1.2
	 [R]emoved bit macros (now using bit.h)
	 [R]emoved uchar (now using uint8_t)
	 [A]dded check for pin definitions
   1.1
     [S]olved bug with minus temperature
   1.02
     [A]dded check of DDR state -> no 250ms wait on 2 and next measures
   1.01
     Tidy source
   1.00
     Initial release

*/

#ifndef DHT_PORT
#warning You didn't choose DHT_PORT and/or other defines. Set to default
#define DHT_PORT PORTB
#define DHT_DDR  DDRB
#define DHT_READ PINB
#define DHT_PIN  0
#endif

/*     Return values
    0, 1 - parity bit
    2 - can't find device
  temp&(1<<15) - sign of temperature (1=='-')   */
uint8_t dht_read(int *temp, int *hum){
    uint8_t data[5], parity, i,j;

    if (bitIsSet(DHT_DDR, DHT_PIN)==0){
    	DHT_PORT|=1<<DHT_PIN;
    	_delay_ms(250);
    }
    DHT_DDR|=1<<DHT_PIN;


    DHT_PORT&=~(1<<DHT_PIN); _delay_ms(1); DHT_PORT|=1<<DHT_PIN; // start cond

    DHT_DDR&=~(1<<DHT_PIN); // input
    _delay_us(50); // waiting for answer

    if (bitIsSet(DHT_READ, DHT_PIN) == 1){ // if no answer
        return 2;
    }

    while (bitIsSet(DHT_READ, DHT_PIN) == 0) { ; }
    while (bitIsSet(DHT_READ, DHT_PIN) == 1) { ; }

    for (j=0; j<5; j++){
        for (i=0; i<8; i++){
            while (bitIsSet(DHT_READ, DHT_PIN)==0) { ; } _delay_us(40);
            if (bitIsSet(DHT_READ, DHT_PIN)==0){
            	bitClear(data[j], 7-i);
            }
            else {
            	bitSet(data[j], 7-i);
            	while (bitIsSet(DHT_READ, DHT_PIN)==1) {;}
            }
        }
    }
    parity=0; for (i=0; i<4; i++) { parity+=data[i]; }
    if (parity==data[4]) parity=1; else parity=0;

    *temp=0; *temp=(data[3]|(data[2]<<8));
    *hum=0; *hum=data[1]|(data[0]<<8);

    return parity;
}


#endif
