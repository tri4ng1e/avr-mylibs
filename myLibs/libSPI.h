/*
    Copyright (c) 2007 Stefan Engelke <mbox@stefanengelke.de>
	Modified by Sky-WaLkeR, May 2013 <alex@naboko.tk>

*/

#ifndef _LIB_SPI_H_
#define _LIB_SPI_H_

#include <avr/io.h>

#define PORT_SPI	PORTB
#define DDR_SPI		DDRB
#define DD_MISO		4
#define DD_MOSI		3
#define DD_SS		2
#define DD_SCK		5

// Initialize pins for spi communication
void spi_init(){
	DDR_SPI &= ~((1<<DD_MOSI)|(1<<DD_MISO)|(1<<DD_SS)|(1<<DD_SCK));
    DDR_SPI |= ((1<<DD_MOSI)|(1<<DD_SS)|(1<<DD_SCK));
    
    SPCR = ((1<<SPE)|               // SPI Enable
            (0<<SPIE)|              // SPI Interupt Enable
            (0<<DORD)|              // Data Order (0:MSB first / 1:LSB first)
            (1<<MSTR)|              // Master/Slave select   
            (0<<SPR1)|(1<<SPR0)|    // SPI Clock Rate
            (0<<CPOL)|              // Clock Polarity (0:SCK low / 1:SCK hi when idle)
            (0<<CPHA));             // Clock Phase (0:leading / 1:trailing edge sampling)
    SPSR = (1<<SPI2X);              // Double Clock Rate
}

// Shift full array through target device
void spi_transfer_sync (uint8_t * dataout, uint8_t * datain, uint8_t len){
	uint8_t i;
	for (i = 0; i < len; i++) {
		SPDR = dataout[i];
		while((SPSR & (1<<SPIF))==0);
		datain[i] = SPDR;
	}
}

// Shift full array to target device without receiving any byte
void spi_transmit_sync (uint8_t *dataout, uint8_t len){
	uint8_t i;
	for (i = 0; i < len; i++) {
		SPDR = dataout[i];
		while((SPSR & (1<<SPIF))==0);
	}
}

// Clocks one byte to target device and returns the received one
uint8_t spi_fast_shift (uint8_t data){
    SPDR = data;
    while((SPSR & (1<<SPIF))==0);
    return SPDR;
}

#endif /* _LIB_SPI_H_ */
