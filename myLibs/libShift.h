#include <avr/io.h>

#ifndef _LIB_SPI_H_
#warning Library "Shift" has dependency from libSPI.h. This lib auto-included.
#include <myLibs/libSPI.h>
#endif

// RESET and LATCH pin of shift registers
#ifndef RST_PIN
#warning You didn't choose RST_PIN and/or other defines. Set to default
#define RST_PIN   1
#define RST_PORT  PORTB
#define LOCK_PIN  0
#define LOCK_PORT PORTB
#endif

// internal use only
void __arr_reverse(uchar word[], uchar len){
    for (int i=0;i<len/2;i++) {
    	word[i]^=word[len-i-1];
    	word[len-i-1]^=word[i];
    	word[i]^=word[len-i-1];
    }
}

void shift_init(){
	RST_PORT|=1<<RST_PIN;
	LOCK_PORT|=1<<LOCK_PIN;
	spi_init();
}

void shift_out(uint8_t arr[], uint8_t cnt){
	LOCK_PORT&=~(1<<LOCK_PIN); 
	RST_PORT&=~(1<<RST_PIN);
	RST_PORT|=1<<RST_PIN;
	__arr_reverse(arr, cnt);
	spi_transmit_sync(arr, cnt);
	LOCK_PORT|=1<<LOCK_PIN;
	__arr_reverse(arr, cnt);
}
