#ifndef _LIB_DS1307_H_
#define _LIB_DS1307_H_

#ifndef _LIB_TWI_H_
#warning "Library \"DS1307\" has dependency from libTWI.h. This lib auto-included."
#include <myLibs/libTWI.h>
#endif

#define DS1307_ADDR 0xd0

#define SECONDS	0x00
#define MINUTES	0x01
#define HOURS	0x02
#define DAY		0x03
#define DATE	0x04
#define MONTH	0x05
#define YEAR	0x06
#define CONTROL	0x07

#define H24 0
#define H12 1


uint8_t bcd2bin(uint8_t x){
    return (((x & 0xF0)>>4)*10 + (x & 0x0F));
}

uint8_t bin2bcd(uint8_t x){
    return ((x%10) | ((x/10)<<4));
}

void rtc_init(uint8_t PeriodSelect=0,   // period on OUT
                                        // 0 - 1 Hz
                                        // 1 - 4096 Hz
                                        // 2 - 8192 Hz
                                        // 3 - 32768 Hz
              uint8_t SQWe=1,           // signal on OUT
              uint8_t OUTlevel=1        // OUT level, if SQWe==0
              ){
    PeriodSelect&=3;
    if (SQWe) PeriodSelect |= 0x10;
    if (OUTlevel) PeriodSelect |= 0x80;
    i2c_init();
    i2c_start(DS1307_ADDR);
    i2c_write(CONTROL);
    i2c_write(PeriodSelect);
    i2c_stop();
}

void rtc_get_time(uint8_t *hour, uint8_t *min, uint8_t *sec){
    i2c_start(DS1307_ADDR);
    i2c_write(0);
    i2c_start(DS1307_ADDR, I2C_READ);
    *sec = bcd2bin(i2c_read());
    *min = bcd2bin(i2c_read());
    *hour = bcd2bin(i2c_read(I2C_NAK));
    i2c_stop();
}

void rtc_set_time(uint8_t hour, uint8_t min, uint8_t sec){
    i2c_start(DS1307_ADDR);
    i2c_write(0);
    i2c_write(bin2bcd(sec));
    i2c_write(bin2bcd(min));
    i2c_write(bin2bcd(hour));
    i2c_stop();
}

void rtc_get_date(uint8_t *date, uint8_t *month, uint8_t *year){
    i2c_start(DS1307_ADDR);
    i2c_write(4);
    i2c_start(DS1307_ADDR, I2C_READ);
    *date=bcd2bin(i2c_read());
    *month=bcd2bin(i2c_read());
    *year=bcd2bin(i2c_read(I2C_NAK));
    i2c_stop();
}

void rtc_set_date(uint8_t date, uint8_t month, uint8_t year){
    i2c_start(DS1307_ADDR);
    i2c_write(4);
    i2c_write(bin2bcd(date));
    i2c_write(bin2bcd(month));
    i2c_write(bin2bcd(year));
    i2c_stop();
}

void rtc_start(uint8_t flag=1){
	i2c_start(DS1307_ADDR);
	i2c_write(SECONDS);
	i2c_start(DS1307_ADDR, I2C_READ);
	uint8_t conf=i2c_read(I2C_NAK);
	i2c_start(DS1307_ADDR);
	i2c_write(SECONDS);
	i2c_write(conf&(0b1111111|(!flag<<7)));
	i2c_stop();
}

void rtc_hour_mode(uint8_t mode=H24){
	i2c_start(DS1307_ADDR);
	i2c_write(HOURS);
	i2c_start(DS1307_ADDR, I2C_READ);
	uint8_t conf=i2c_read(I2C_NAK);
	i2c_start(DS1307_ADDR);
	i2c_write(HOURS);
	i2c_write(conf&(0b1111111|(mode<<6)));
	i2c_stop();
}

#endif
