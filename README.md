# AVR myLibs

## Welcome

This repo contains some of my libs. Not all libs written by me - I leave copyrights in lib comments.

Now you can find here libs for RTC module (DS1307), DHT22/AM2303, nRF2401(+) Transceiver, easy work with I/O ports and bits (thanks to Neiver @ easyelectronics.ru) and many other.

## Attention

First of all, sorry for my English and code style.  
I don't take any responsibility for not working libs and devices.  
I use the AVR-GCC v4.8.0 compiler. You can take sources and compile it yourself or search precompiled version (as I did).  

I hold all my libraries in "myLibs" folder (which is in "include" folder of compiler).  
In all of my AVR projects and libs I include this libraries as *"#include <myLibs/some_library.h>*  
If you put libraries in another folder, you should edit paths *manually*. Some libs have reendence from another lib, if you can't compile it, check paths.  
Some libs tested only on ATmega8 and ATmega32. If they don't work, please send me "Issue" with detailed description and I try to solve it.